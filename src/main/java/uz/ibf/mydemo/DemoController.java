package uz.ibf.mydemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mexanik_PC on 21/01/2024 09:14
 * @project mydemo
 */
@RestController
public class DemoController {

    @GetMapping
    public String demo(){
        return "Salom Hammaga";
    }
}
